Required Libraries
======================
Gson jar file (already included in ./src/lib/gson-2.6.2.jar)


How to Run
======================
All java files were compiled using:
>	java version "1.8.0_65"  
>	Java(TM) SE Runtime Environment (build 1.8.0_65-b17)  
>	Java HotSpot(TM) 64-Bit Server VM (build 25.65-b01, mixed mode)  
	
Run the program by typing in the command line:
>	bash  run.sh

If the program needs to be recompiled, type in the command line:
>	javac -cp ./src/lib/gson-2.6.2.jar -d ./src/classes ./src/main/*.java
