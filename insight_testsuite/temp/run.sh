#!/usr/bin/env bash

# Running program with input file ./tweet_input/tweets.txt
# and output file ./tweet_output/output.txt
java  -cp ./src/lib/gson-2.6.2.jar:./src/classes main.AverageDegree ./tweet_input/tweets.txt ./tweet_output/output.txt
