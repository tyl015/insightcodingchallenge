package tests;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import main.Tweet;
import main.TwitterParser;

import org.junit.Test;

public class TwitterParserTest {

	@Test
	public void testParseHashtags() {
		TwitterParser myParser = new TwitterParser();
		// JSON message with no hashtags
		String message = "{\"created_at\":\"Fri Oct 30 15:29:45 +0000 2015\",\"entities\":{\"hashtags\":[]}}";
		Tweet myTweet = myParser.parseHashtags(message);
		
		SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.US);
		Date date = null;
		try {
			date = sdf.parse("Fri Oct 30 15:29:45 +0000 2015");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		Date resultDate = myTweet.getDate();
		assertEquals(resultDate, date);
		
		String[] hashtags = myTweet.getHashtags();
		assertEquals(hashtags.length, 0);
		
		// JSON message with 3 hashtags but only 2 unique hashtags
		message = "{\"created_at\":\"Fri Oct 30 15:29:45 +0000 2015\",\"entities\":{\"hashtags\":[{\"text\":\"hashtag1\",\"indices\":[1,1]},{\"text\":\"hashtag1\",\"indices\":[1,1]},{\"text\":\"hashtag2\",\"indices\":[1,1]}]}}";
		myTweet = myParser.parseHashtags(message);
		
		hashtags = myTweet.getHashtags();
		assertEquals(hashtags.length, 2);
		
		Arrays.sort(hashtags);
		assertEquals(hashtags[0], "hashtag1");
		assertEquals(hashtags[1], "hashtag2");
		
		// JSON rate-limiting message
		message = "{\"limit\":{\"track\":5,\"timestamp_ms\":\"123456\"}}";
		myTweet = myParser.parseHashtags(message);
		
		assertNull(myTweet);
	}
}
