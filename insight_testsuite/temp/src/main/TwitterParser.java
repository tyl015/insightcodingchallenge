package main;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class TwitterParser {
	private JsonParser parser = new JsonParser();
	private Date date = null;
	private SimpleDateFormat sdf = new SimpleDateFormat(
			"EEE MMM dd HH:mm:ss Z yyyy", Locale.US);

	public Tweet parseHashtags(String line) {

		HashSet<String> hashtags; // using HashSet to avoid duplicate hashtags
		JsonObject obj;
		JsonElement elem;
		JsonArray hashtagsArray;
		JsonObject hashtagObj;
		int i;

		obj = parser.parse(line).getAsJsonObject();

		elem = obj.get("limit");
		// rate limiting message instead of actual tweet so return null
		if (elem != null) {
			return null;
		}

		// parsing date
		try {
			date = sdf.parse(obj.get("created_at").getAsString());
		} catch (ParseException e) {
			e.printStackTrace();
		}

		// parsing hashtags
		hashtagsArray = obj.getAsJsonObject("entities").get("hashtags")
				.getAsJsonArray();
		hashtags = new HashSet<String>();
		for (i = 0; i < hashtagsArray.size(); i++) {
			hashtagObj = hashtagsArray.get(i).getAsJsonObject();
			hashtags.add(hashtagObj.get("text").getAsString());
		}

		// create new tweet and return it
		return new Tweet(date, hashtags);
	}
}
