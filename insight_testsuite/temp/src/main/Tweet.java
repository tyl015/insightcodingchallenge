package main;

import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;

public class Tweet implements Comparator<Tweet>, Comparable<Tweet> {
	Date date;
	String[] hashtags;

	public Tweet(Date date, HashSet<String> hashtags) {
		this.date = date;
		this.hashtags = hashtags.toArray(new String[hashtags.size()]);
	}

	public Date getDate() {
		return date;
	}

	public String[] getHashtags() {
		return hashtags;
	}

	@Override
	public int compareTo(Tweet t) {
		return this.getDate().compareTo(t.getDate());
	}

	@Override
	public int compare(Tweet x, Tweet y) {
		return x.getDate().compareTo(y.getDate());
	}
}
