package main;

import java.util.Date;
import java.util.PriorityQueue;

public class Timeline {
	private int timeWindow;
	private PriorityQueue<Tweet> tweetQueue = new PriorityQueue<Tweet>();
	private Graph myGraph = new Graph();
	private Date latestTweetTime;

	public Timeline(int timeWindow) {
		this.timeWindow = timeWindow;
	}

	public Date getLatestTweetTime() {
		return latestTweetTime;
	}

	public PriorityQueue<Tweet> getTweetQueue() {
		return tweetQueue;
	}

	public void addTweet(Tweet tweet) throws Exception {

		// skip adding tweet to queue if older than timeWindow milliseconds
		// compared
		// to latest tweet time
		if (latestTweetTime != null
				&& (latestTweetTime.getTime() - tweet.getDate().getTime() > timeWindow)) {
			return;
		}

		// update latest tweet time if new tweet is the latest tweet
		if (latestTweetTime == null || tweet.getDate().after(latestTweetTime)) {
			latestTweetTime = tweet.getDate();
		}

		// add tweet to queue
		tweetQueue.add(tweet);

		// add tweet to graph
		myGraph.addTweet(tweet);

		// remove tweets older than latest tweet time by timeWindow
		// milliseconds
		while (latestTweetTime.getTime()
				- tweetQueue.peek().getDate().getTime() > timeWindow) {
			// delete old tweet from queue and graph
			myGraph.deleteTweet(tweetQueue.poll());
		}
	}

	public double getAverageDegree() {
		return myGraph.computeAverageDegree();
	}

}