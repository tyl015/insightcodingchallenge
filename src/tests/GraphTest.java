package tests;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.HashSet;

import main.Graph;
import main.Tweet;

import org.junit.Test;

public class GraphTest {

	private HashSet<String> newHashSet(String... strings) {
		HashSet<String> set = new HashSet<String>();

		for (String s : strings) {
			set.add(s);
		}
		return set;
	}

	@Test
	public void testAddTweet() {
		Graph myGraph = new Graph();
		HashSet<String> hashtags;
		Date date = new Date();
		Tweet tweet;
		double avgDegree;

		hashtags = newHashSet("a", "b");
		tweet = new Tweet(date, hashtags);

		myGraph.addTweet(tweet);
		avgDegree = myGraph.computeAverageDegree();
		assertEquals(avgDegree, 1, 0);

		// Adding tweet with same connections as before so the average degree
		// should remain the same
		hashtags = newHashSet("a", "b");
		tweet = new Tweet(date, hashtags);

		myGraph.addTweet(tweet);
		avgDegree = myGraph.computeAverageDegree();
		assertEquals(avgDegree, 1, 0);

		hashtags = newHashSet("b", "c", "d");
		tweet = new Tweet(date, hashtags);

		myGraph.addTweet(tweet);
		avgDegree = myGraph.computeAverageDegree();
		assertEquals(avgDegree, 2, 0);

		// Adding tweet with only one hashtag so it should not be processed and
		// the average degree should remain the same
		hashtags = newHashSet("e");
		tweet = new Tweet(date, hashtags);

		myGraph.addTweet(tweet);
		avgDegree = myGraph.computeAverageDegree();
		assertEquals(avgDegree, 2, 0);

		hashtags = newHashSet("e", "f", "g", "h");
		tweet = new Tweet(date, hashtags);

		myGraph.addTweet(tweet);
		avgDegree = myGraph.computeAverageDegree();
		assertEquals(avgDegree, (20.0 / 8), 0);
	}

	@Test
	public void testDeleteTweet() throws Exception {
		Graph myGraph = new Graph();
		HashSet<String> hashtags;
		Date date = new Date();
		double avgDegree;
		Tweet tweet;
		Tweet tweet2;
		Tweet tweet3;

		// General test
		hashtags = newHashSet("a", "b", "c");
		tweet = new Tweet(date, hashtags);

		hashtags = newHashSet("d", "e");
		tweet2 = new Tweet(date, hashtags);

		myGraph.addTweet(tweet);
		myGraph.addTweet(tweet2);
		avgDegree = myGraph.computeAverageDegree();
		assertEquals(avgDegree, (8.0 / 5), 0);

		myGraph.deleteTweet(tweet);
		avgDegree = myGraph.computeAverageDegree();
		assertEquals(avgDegree, 1, 0);

		myGraph.deleteTweet(tweet2);
		avgDegree = myGraph.computeAverageDegree();
		assertEquals(avgDegree, 0, 0);

		// Connected tweets test
		hashtags = newHashSet("a", "b");
		tweet = new Tweet(date, hashtags);

		hashtags = newHashSet("b", "c");
		tweet2 = new Tweet(date, hashtags);

		hashtags = newHashSet("c", "a");
		tweet3 = new Tweet(date, hashtags);

		myGraph.addTweet(tweet);
		myGraph.addTweet(tweet2);
		myGraph.addTweet(tweet3);
		avgDegree = myGraph.computeAverageDegree();
		assertEquals(avgDegree, 2, 0);

		myGraph.deleteTweet(tweet);
		avgDegree = myGraph.computeAverageDegree();
		assertEquals(avgDegree, (4.0 / 3), 0);

		myGraph.deleteTweet(tweet2);
		avgDegree = myGraph.computeAverageDegree();
		assertEquals(avgDegree, 1, 0);

		myGraph.deleteTweet(tweet3);
		avgDegree = myGraph.computeAverageDegree();
		assertEquals(avgDegree, 0, 0);

		// Another connected tweets test
		hashtags = newHashSet("a", "b", "c");
		tweet = new Tweet(date, hashtags);

		hashtags = newHashSet("b", "c", "d");
		tweet2 = new Tweet(date, hashtags);

		myGraph.addTweet(tweet);
		myGraph.addTweet(tweet2);
		avgDegree = myGraph.computeAverageDegree();
		assertEquals(avgDegree, (10.0 / 4), 0);

		myGraph.deleteTweet(tweet);
		avgDegree = myGraph.computeAverageDegree();
		assertEquals(avgDegree, 2, 0);

		myGraph.deleteTweet(tweet2);
		avgDegree = myGraph.computeAverageDegree();
		assertEquals(avgDegree, 0, 0);

		// Duplicate tweets test
		hashtags = newHashSet("a", "b");
		tweet = new Tweet(date, hashtags);

		hashtags = newHashSet("b", "a");
		tweet2 = new Tweet(date, hashtags);

		myGraph.addTweet(tweet);
		myGraph.addTweet(tweet2);
		avgDegree = myGraph.computeAverageDegree();
		assertEquals(avgDegree, 1, 0);

		myGraph.deleteTweet(tweet);
		avgDegree = myGraph.computeAverageDegree();
		assertEquals(avgDegree, 1, 0);

		myGraph.deleteTweet(tweet2);
		avgDegree = myGraph.computeAverageDegree();
		assertEquals(avgDegree, 0, 0);

		// Testing tweets with only one hashtag and zero connections
		hashtags = newHashSet("a", "b");
		tweet = new Tweet(date, hashtags);

		hashtags = newHashSet("a");
		tweet2 = new Tweet(date, hashtags);

		hashtags = newHashSet("b");
		tweet3 = new Tweet(date, hashtags);

		myGraph.addTweet(tweet);
		myGraph.addTweet(tweet2);
		myGraph.addTweet(tweet3);
		avgDegree = myGraph.computeAverageDegree();
		assertEquals(avgDegree, 1, 0);

		myGraph.deleteTweet(tweet);
		avgDegree = myGraph.computeAverageDegree();
		assertEquals(avgDegree, 0, 0);
	}
}
