package tests;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.HashSet;
import java.util.PriorityQueue;

import main.Timeline;
import main.Tweet;

import org.junit.Test;

public class TimelineTest {

	@Test
	public void testAddTweet() throws Exception {
		Timeline myTimeline = new Timeline(10); // 10 millisecond time window

		HashSet<String> hashtags = new HashSet<String>();

		Date myDate = new Date();
		Tweet myTweet = new Tweet(myDate, hashtags);
		myTimeline.addTweet(myTweet);

		Date resultDate = myTimeline.getLatestTweetTime();
		assertEquals(myDate, resultDate);

		PriorityQueue<Tweet> resultQueue = myTimeline.getTweetQueue();
		assertEquals(resultQueue.size(), 1);

		//
		// Adding tweet2 with date 5 milliseconds newer than original tweet
		//
		Date myDate2 = new Date();
		myDate2.setTime(myDate.getTime() + 5);
		Tweet myTweet2 = new Tweet(myDate2, hashtags);
		myTimeline.addTweet(myTweet2);

		// This tweet should have the latest time
		resultDate = myTimeline.getLatestTweetTime();
		assertEquals(myDate2, resultDate);

		// All tweets within time window so size should increase by 1
		resultQueue = myTimeline.getTweetQueue();
		assertEquals(resultQueue.size(), 2);

		//
		// Adding tweet3 with date 9 milliseconds newer than original tweet
		//
		Date myDate3 = new Date();
		myDate3.setTime(myDate.getTime() + 9);
		Tweet myTweet3 = new Tweet(myDate3, hashtags);
		myTimeline.addTweet(myTweet3);

		// This tweet should have the latest time
		resultDate = myTimeline.getLatestTweetTime();
		assertEquals(myDate3, resultDate);

		// All tweets within time window so size should increase by 1
		resultQueue = myTimeline.getTweetQueue();
		assertEquals(resultQueue.size(), 3);

		//
		// Adding tweet4 with date 6 milliseconds newer than original tweet
		//
		Date myDate4 = new Date();
		myDate4.setTime(myDate.getTime() + 6);
		Tweet myTweet4 = new Tweet(myDate4, hashtags);
		myTimeline.addTweet(myTweet4);

		// Tweet3 should have the latest time
		resultDate = myTimeline.getLatestTweetTime();
		assertEquals(myDate3, resultDate);

		// All tweets within time window so size should increase by 1
		resultQueue = myTimeline.getTweetQueue();
		assertEquals(resultQueue.size(), 4);

		//
		// Adding tweet5 with date 2 milliseconds older than original tweet
		//
		Date myDate5 = new Date();
		myDate5.setTime(myDate.getTime() - 2);
		Tweet myTweet5 = new Tweet(myDate5, hashtags);
		myTimeline.addTweet(myTweet5);

		// Tweet3 should have the latest time
		resultDate = myTimeline.getLatestTweetTime();
		assertEquals(myDate3, resultDate);

		// This tweet outside time window so size should stay the same
		resultQueue = myTimeline.getTweetQueue();
		assertEquals(resultQueue.size(), 4);

		//
		// Adding tweet6 with same date as original tweet
		//
		Date myDate6 = new Date();
		myDate6.setTime(myDate.getTime());
		Tweet myTweet6 = new Tweet(myDate6, hashtags);
		myTimeline.addTweet(myTweet6);

		// Tweet3 should have the latest time
		resultDate = myTimeline.getLatestTweetTime();
		assertEquals(myDate3, resultDate);

		// All tweets within time window so size should increase by 1
		resultQueue = myTimeline.getTweetQueue();
		assertEquals(resultQueue.size(), 5);

		//
		// Adding tweet7 with date 11 milliseconds newer than original tweet
		//
		Date myDate7 = new Date();
		myDate7.setTime(myDate.getTime() + 11);
		Tweet myTweet7 = new Tweet(myDate7, hashtags);
		myTimeline.addTweet(myTweet7);

		// This tweet should have the latest time
		resultDate = myTimeline.getLatestTweetTime();
		assertEquals(myDate7, resultDate);

		// Original tweet and tweet6 are outside time window so size should
		// decrease by 1
		resultQueue = myTimeline.getTweetQueue();
		assertEquals(resultQueue.size(), 4);

		//
		// Adding tweet8 with date 30 milliseconds newer than original tweet
		//
		Date myDate8 = new Date();
		myDate8.setTime(myDate.getTime() + 30);
		Tweet myTweet8 = new Tweet(myDate8, hashtags);
		myTimeline.addTweet(myTweet8);

		// This tweet should have the latest time
		resultDate = myTimeline.getLatestTweetTime();
		assertEquals(myDate8, resultDate);

		// All other tweets are ouside time window so size should be 1
		resultQueue = myTimeline.getTweetQueue();
		assertEquals(resultQueue.size(), 1);
	}
}
