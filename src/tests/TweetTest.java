package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;

import main.Tweet;

import org.junit.Test;

public class TweetTest {

	@Test
	public void testTweetConstructor() {
		Date date = new Date();
		HashSet<String> hashtags = new HashSet<String>();
		hashtags.add("b");
		hashtags.add("a");
		hashtags.add("c");
		Tweet myTweet = new Tweet(date, hashtags);

		Date innerDate = myTweet.getDate();
		assertEquals(innerDate.getTime(), date.getTime());

		String[] innerArray = myTweet.getHashtags();
		assertEquals(innerArray.length, 3);

		Arrays.sort(innerArray);
		assertEquals(innerArray[0], "a");
		assertEquals(innerArray[1], "b");
		assertEquals(innerArray[2], "c");
	}

	@Test
	public void testCompareTo() {
		Date earlierDate = new Date();
		// wait a little bit so second date is later than first date
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Date laterDate = new Date();
		HashSet<String> hashtags = new HashSet<String>();
		Tweet earlierTweet = new Tweet(earlierDate, hashtags);
		Tweet laterTweet = new Tweet(laterDate, hashtags);

		int result = earlierTweet.compareTo(laterTweet);
		assertTrue(result < 0);
	}

	@Test
	public void testCompare() {
		Date earlierDate = new Date();
		// wait a little bit so second date is later than first date
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Date laterDate = new Date();
		HashSet<String> hashtags = new HashSet<String>();
		Tweet earlierTweet = new Tweet(earlierDate, hashtags);
		Tweet laterTweet = new Tweet(laterDate, hashtags);

		int result = earlierTweet.compare(earlierTweet, laterTweet);
		assertTrue(result < 0);
	}

}
