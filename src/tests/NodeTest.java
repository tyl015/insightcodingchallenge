package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import main.Node;

import org.junit.Test;

public class NodeTest {

	@Test
	public void testNodeConstructor() {
		Node myNode = new Node("test");

		String result = myNode.getName();
		assertEquals(result, "test");

		int result2 = myNode.getCount();
		assertEquals(result2, 1);
	}

	@Test
	public void testAddNeighbor() {
		Node myNode = new Node("test");

		int result = myNode.addNeighbor("a");
		assertEquals(result, 1);

		result = myNode.addNeighbor("b");
		assertEquals(result, 1);

		// already added "a" so adding another "a" should not increase the
		// node's degree so the method should return 0
		result = myNode.addNeighbor("a");
		assertEquals(result, 0);

		// already added "b" so adding another "b" should not increase the
		// node's degree so the method should return 0
		result = myNode.addNeighbor("b");
		assertEquals(result, 0);
	}

	@Test
	public void testRemoveNeighbor() {
		Node myNode = new Node("test");

		myNode.addNeighbor("a");
		myNode.addNeighbor("a");
		myNode.addNeighbor("a");

		myNode.addNeighbor("b");
		myNode.addNeighbor("b");

		myNode.addNeighbor("c");

		myNode.addNeighbor("d");

		int result;

		try {
			result = myNode.removeNeighbor("a");
			assertEquals(result, 0);

			result = myNode.removeNeighbor("a");
			assertEquals(result, 0);

			// added 3 "a" and removed 2 "a" so removing another "a" should
			// decrease the node's degree and have the method return 1
			result = myNode.removeNeighbor("a");
			assertEquals(result, 1);

			result = myNode.removeNeighbor("b");
			assertEquals(result, 0);

			// added 2 "b" and removed 1 "b" so removing another "b" should
			// decrease the node's degree and have the method return 1
			result = myNode.removeNeighbor("b");
			assertEquals(result, 1);

			// only added 1 "c" so removing another "c" should decrease the
			// node's degree and have the method return 1
			result = myNode.removeNeighbor("c");
			assertEquals(result, 1);
		} catch (Exception e) {
			fail("Expected no Exceptions");
		}

		// added 3 "a" and removed 3 "a" so trying to remove another "a" should
		// throw an exception
		try {
			result = myNode.removeNeighbor("a");
			fail("Expected an Exception");
		} catch (Exception e) {
			assertEquals(e.getMessage(),
					"Tried to remove non-existant neighbor: a");
		}
	}
}
