package main;

import java.util.HashMap;

public class Node {
	private String name;
	private int count = 1; // used to keep track of whether the node exists
	private HashMap<String, Integer> neighbors = new HashMap<String, Integer>();
	private int degree = 0;

	public Node(String name) {
		this.name = name;
	}

	// returns 1 if degree increased by one and 0 if degree stayed the same
	public int addNeighbor(String nodeName) {
		if (neighbors.get(nodeName) != null) {
			neighbors.put(nodeName, neighbors.get(nodeName) + 1);
			return 0;
		} else {
			neighbors.put(nodeName, 1);
			degree++;
			return 1;
		}
	}

	// returns 1 if degree decreased by one and 0 if degree stayed the same
	public int removeNeighbor(String nodeName) throws Exception {
		if (neighbors.get(nodeName) == null) {
			throw new Exception("Tried to remove non-existant neighbor: "
					+ nodeName);
		}
		if (neighbors.get(nodeName) == 1) {
			degree--;
			neighbors.remove(nodeName);
			return 1;
		} else {
			neighbors.put(nodeName, neighbors.get(nodeName) - 1);
			return 0;
		}
	}

	public String getName() {
		return name;
	}

	public int getDegree() {
		return degree;
	}

	public int getCount() {
		return count;
	}

	public void increaseCount() {
		count++;
	}

	public void decreaseCount() {
		count--;
	}
}