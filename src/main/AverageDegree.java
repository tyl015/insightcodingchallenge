package main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class AverageDegree {
	private static final int TIME_WINDOW = 60000; // 60 seconds in milliseconds

	public static void main(String[] args) {

		// for timing purposes
		long startTime = System.nanoTime();

		try {
			FileReader fr = new FileReader(args[0]);
			BufferedReader br = new BufferedReader(fr);
			String line;

			TwitterParser myParser = new TwitterParser();
			Tweet tweet;

			Timeline myTimeline = new Timeline(TIME_WINDOW);
			double avgDegree;

			// set decimal format to truncate at 2 decimal places
			DecimalFormat df = new DecimalFormat("0.00");
			df.setRoundingMode(RoundingMode.DOWN);

			PrintWriter writer = new PrintWriter(args[1]);

			while ((line = br.readLine()) != null) {

				// parse tweet object from message
				tweet = myParser.parseHashtags(line);

				// skip processing for rate-limiting messages
				if (tweet == null) {
					continue;
				}

				// add tweet to timeline
				myTimeline.addTweet(tweet);

				// get new average degree from timeline
				avgDegree = myTimeline.getAverageDegree();

				// write new average degree to file
				writer.println(df.format(avgDegree));
			}

			writer.close();
			br.close();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// for timing purposes
		long endTime = System.nanoTime();
		long difference = endTime - startTime;
		System.out.println("Elapsed milliseconds: " + difference / 1000000);

	}
}
