package main;

import java.util.HashMap;

public class Graph {
	private HashMap<String, Node> hashtagsMap = new HashMap<String, Node>();
	private int totalDegree = 0;
	private int totalNodes = 0;

	public void addTweet(Tweet tweet) {

		Node tempNode;
		String[] hashtags;
		int i;
		int j;

		hashtags = tweet.getHashtags();

		// if the tweet does not contain more than 1 hashtag then skip it
		if (hashtags.length <= 1) {
			return;
		}

		for (i = 0; i < hashtags.length; i++) {
			if ((tempNode = hashtagsMap.get(hashtags[i])) == null) {
				// create new node
				tempNode = new Node(hashtags[i]);
				// add all of its neighbors
				for (j = 0; j < hashtags.length; j++) {
					if (j != i) {
						tempNode.addNeighbor(hashtags[j]);
					}
				}
				// add node to hash map
				hashtagsMap.put(tempNode.getName(), tempNode);
				// update total degree
				totalDegree += tempNode.getDegree();
				// update total number of nodes
				totalNodes++;
			} else {
				// increment existing node
				tempNode.increaseCount();
				// add possible new neighbors
				for (j = 0; j < hashtags.length; j++) {
					if (j != i) {
						// update total degree
						totalDegree += tempNode.addNeighbor(hashtags[j]);
					}
				}
			}
		}
	}

	public void deleteTweet(Tweet tweet) throws Exception {

		Node tempNode;
		String[] hashtags;
		int i;
		int j;

		hashtags = tweet.getHashtags();

		// if the tweet does not contain more than 1 hashtag then skip it
		if (hashtags.length <= 1) {
			return;
		}

		for (i = 0; i < hashtags.length; i++) {
			tempNode = hashtagsMap.get(hashtags[i]);
			tempNode.decreaseCount();

			// decrement neighbor counts to see if any edges disappear
			for (j = 0; j < hashtags.length; j++) {
				if (j != i) {
					// update total degree
					totalDegree -= tempNode.removeNeighbor(hashtags[j]);
				}
			}

			// if the node no longer exists
			if (tempNode.getCount() == 0) {
				// remove node from hash map
				hashtagsMap.remove(tempNode.getName());
				// update total number of nodes
				totalNodes--;
			}
		}
	}

	public double computeAverageDegree() {
		// if total nodes is zero then there are no connections so return 0
		if (totalNodes == 0) {
			return 0;
		} else {
			return (double) totalDegree / totalNodes;
		}
	}
}
